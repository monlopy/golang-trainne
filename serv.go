package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

//struct
type Todo struct {
	Age       int
	Completed bool
	Due       time.Time
}

type Todos []Todo

//send random ineger events
func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Println(w, "Start ")
	todos := Todos{
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
		Todo{Age: rand.Intn(100)},
	}
	//Using CLI
	/*var n int;
	fmt.Scanf("%d",&n)
	json.NewEncoder(w).Encode(todos[0:n])*/
	json.NewEncoder(w).Encode(todos[0:4])

}

//limit requests per user
var limiter = rate.NewLimiter(1, 50)

func limit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if limiter.Allow() == false {
			http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(w, r)
	})
}

//main func
func main() {
	wg := new(sync.WaitGroup)
	wg.Add(4)
	go func() {
		http.ListenAndServe(":8080", limit(http.NewServeMux()))
		wg.Done()
	}()
	http.HandleFunc("/", handle)
	wg.Wait()
}
