package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func Tes() {

	res, err := http.Get("http://localhost:8080/")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Println(string(body))
}
func main() {
	log.Println(Tes)
}
